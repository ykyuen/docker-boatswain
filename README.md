# Boatswain as Docker container

Official repository of Boatswain in Docker container. For more information please refer to our [official homepage](https://boatswain.io/).

If you have any questions about Boatswain, please feel free to visit our [repository at gitlab](https://gitlab.com/ykyuen/docker-boatswain) and create an issue.

## Limitation when running Boatswain as container

- The `Disk Utilization` graph in the `Host Status` page could not show the correct values.
- Cannot read the network statistics of the Boatswain container.

## Run the Bostswain container

Before starting Boatswain, you need to register an account @ [Boatswain.io](https://app.boatswain.io/pages/register.html) to get the `BOATSWAIN_TOKEN`.

### By docker command

```
docker run --name boatswain \
  -e "BOATSWAIN_TOKEN=<TO_BE_REPLACED>" \
  -e "BOATSWAIN_PERIOD=default" \
  -e "BOATSWAIN_SEND_LOG=true" \
  -v /var/run/docker.sock:/var/run/docker.sock \
  --pid host \
  --network host \
  --cap-add NET_ADMIN \
  --restart always \
  -d boatswainio/boatswain:<latest or tag> boatswain
```

### By docker compose

Create the following `docker-compose.yml` and fill in the `BOATSWAIN_TOKEN`.

```
version: '3.2'
services:
  boatswain:
    image: boatswainio/boatswain:<latest or tag>
    container_name: boatswain
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    pid: "host"
    network_mode: "host"
    cap_add:
      - NET_ADMIN
    restart: always
    environment:
      BOATSWAIN_TOKEN: <TO_BE_REPLACED>
      BOATSWAIN_PERIOD: default
      BOATSWAIN_SEND_LOG: "true"
```

Then you can start the Boatswain container by `docker-compose up -d`.

## Other configuration

The following is the Boatswain configuration file for your reference.

```
# Please register and get the token from https://app.boatswain.io/
token: token_here
# Period is the collection time interval of the host and docker metrics.
# CPU load could be reduced by using longer period.
#   - short   = 10s
#   - default = 30s
#   - long    = 60s
period: default
# Send Log is the flag which determines if you would like to send the docker
# container logs to Boatswain.
#   - true
#   - false
send_log: true
```

Let's say if you want to alter the `period` settings, you could update the command or the docker compose file by editing the `BOATSWAIN_PERIOD` environment variable.
