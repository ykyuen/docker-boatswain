FROM debian:9.4

RUN \
  apt-get update && apt-get install -y wget sudo gnupg2 gettext-base

RUN \
  wget -O- http://repo.boatswain.io/boatswain-signing.gpg | sudo apt-key add - && \
  echo 'deb http://repo.boatswain.io/packages/apt stable main' | sudo tee /etc/apt/sources.list.d/boatswain.list && \
  apt-get update && \
  apt-get install -y --allow-unauthenticated boatswain=0.2.13

COPY ./etc/boatswain.yml.template /etc/boatswain.yml.template

COPY ./entrypoint.sh /entrypoint.sh
RUN chmod a+x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ["boatswain"]
