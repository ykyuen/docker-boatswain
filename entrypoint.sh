#!/bin/bash
set -e

# Bash, in case we need the container ac-hoc.
if [ "$1" = 'bash' ]; then
  exec "$@"
fi

# Start Boatswain
if [ "$1" = 'boatswain' ]; then
  envsubst '$BOATSWAIN_TOKEN $BOATSWAIN_PERIOD $BOATSWAIN_SEND_LOG' < /etc/boatswain.yml.template > /etc/boatswain.yml
  exec /usr/bin/boatswain -f /etc/boatswain.yml
fi
